﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimeGraphServer.GraphQL
{
    public class ProjectType : ObjectType<Project>
    {
    }
    public class ProductType : ObjectType<Product>
    {
    }

    public class DepartmentType : ObjectType<Department>
    {
    }
}
