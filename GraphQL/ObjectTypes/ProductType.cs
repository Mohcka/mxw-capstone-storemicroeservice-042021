using HotChocolate.Types;

public class ProductType : ObjectType<Product>
{
    protected override void Configure(IObjectTypeDescriptor<Product> descriptor)
    {
        descriptor.Field(f => f.Id).Type<IdType>();
        descriptor.Field(f => f.Name).Type<StringType>();
        descriptor.Field(f => f.Department);
    }
}
