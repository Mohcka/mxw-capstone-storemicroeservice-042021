﻿using HotChocolate;
using HotChocolate.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Query
{
    /// <summary>
    /// Defined query to fetch all known projects.
    /// </summary>
    /// <param name="dbcontext">The database context used to fetch the projects</param>
    /// <returns>A list of projects</returns>
    [UseDbContext(typeof(StoreServiceContext))]
    public IQueryable<Project> GetProjects([ScopedService] StoreServiceContext dbcontext)
        => dbcontext.Projects;

    [UseDbContext(typeof(StoreServiceContext))]
    public Project GetProject([ScopedService] StoreServiceContext dbContext, int id)
        => dbContext.Projects.FirstOrDefault(p => p.Id == id);

    // * PRODUCTS
    [UseDbContext(typeof(StoreServiceContext))]
    public IQueryable<Product> GetProducts([ScopedService] StoreServiceContext dbContext)
        => dbContext.Products.Include(p => p.Department);

    [UseDbContext(typeof(StoreServiceContext))]
    public Product GetProduct([ScopedService] StoreServiceContext dbContext, Guid id)
        => dbContext.Products.FirstOrDefault(p => p.Id == id);

    // * DEPARTMENTS
    [UseDbContext(typeof(StoreServiceContext))]
    public IQueryable<Department> GetDepartments([ScopedService] StoreServiceContext dbContext)
        => dbContext.Departments.Include(d => d.Products);

    [UseDbContext(typeof(StoreServiceContext))]
    public Department GetDepartment([ScopedService] StoreServiceContext dbContext, Guid id)
        => dbContext.Departments.Include(d => d.Products).FirstOrDefault(d => d.Id == id);

    [UseDbContext(typeof(StoreServiceContext))]
    public IQueryable<Department> GetDepartmentsByRange([ScopedService] StoreServiceContext dbContext, Guid[] ids)
        => dbContext.Departments.Include(d => d.Products).Where(d => ids.Contains(d.Id));
}
