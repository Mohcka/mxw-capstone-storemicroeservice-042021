using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

public class Mutation
{
    [UseDbContext(typeof(StoreServiceContext))]
    public async Task<Project> CreateProject([ScopedService] StoreServiceContext dbContext, string name, string createdBy)
    {
        var proj = new Project
        {
            Name = name,
            CreatedBy = createdBy
        };

        dbContext.Projects.Add(proj);
        await dbContext.SaveChangesAsync();
        return proj;
    }

    [UseDbContext(typeof(StoreServiceContext))]
    public async Task<Project> UpdateProject([ScopedService] StoreServiceContext dbContext, int id, string name, string createdBy)
    {
        var projToUpdate = dbContext.Projects.FirstOrDefault(p => p.Id == id);

        projToUpdate.Name = name;
        projToUpdate.CreatedBy = createdBy;

        dbContext.Projects.Update(projToUpdate);
        await dbContext.SaveChangesAsync();
        return projToUpdate;
    }

    [UseDbContext(typeof(StoreServiceContext))]
    public async Task<Product> CreateProduct([ScopedService] StoreServiceContext dbContext, Guid departmentId, string name, int quantity, decimal price)
    {
        var dept = await dbContext.Departments.FirstOrDefaultAsync(d => d.Id == departmentId);

        var prod = new Product
        {
            Name = name,
            Quantity = quantity,
            Price = price,
            Department = dept
        };

        await dbContext.Products.AddAsync(prod);
        await dbContext.SaveChangesAsync();
        return prod;
    }
}