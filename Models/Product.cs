using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Product
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public int Quantity { get; set; }
    [Column(TypeName = "decimal(1,1)")]
    public decimal Price { get; set; }
    public Department Department { get; set; }
}