using System;
using System.Collections.Generic;

public class Department {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<Product> Products { get; set; }
}