using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class StoreServiceContext : DbContext
{
    public StoreServiceContext(DbContextOptions<StoreServiceContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    public DbSet<Project> Projects { get; set; }
    public DbSet<TimeLog> TimeLogs { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Department> Departments { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {


        var dept1Id = new Guid("0763a35a-b8a7-4421-8ee1-e96d0f7852eb");
        var dept2Id = new Guid("ef61f94e-451e-4173-8cfc-80415bd54f8d");
        var products = new [] {
            new
            {
                Id = new Guid("6af1dfc2-c903-4bfb-b4e5-8c17abdc7173"),
                Name = "Chair",
                Quantity = 4,
                Price = (decimal)2.99,
                DepartmentId = dept1Id
            },
            new
            {
                Id = new Guid("cea10eff-63d0-4806-97f8-744f4c703321"),
                Name = "Pen",
                Quantity = 6,
                Price = (decimal)3.99,
                DepartmentId = dept2Id
            },
            new
            {
                Id = new Guid("d4976e92-d20b-470a-bb79-cc5324501098"),
                Name = "Car",
                Quantity = 2,
                Price = (decimal)4.99,
                DepartmentId = dept2Id
            }
        };

        modelBuilder.Entity<Department>(b =>
        {
            b.HasData(new Department
            {
                Id = dept1Id,
                Name = "Store 1"
            }, new Department
            {
                Id = dept2Id,
                Name = "Hello Store"
            });
        });

        modelBuilder.Entity<Product>(b =>
        {
            b.HasOne(p => p.Department).WithMany(d => d.Products);

            b.HasData(products);
        });

        modelBuilder.Entity<Project>().HasData(new Project
        {
            CreatedBy = "Giorgi",
            Id = 1,
            Name = "Migrate to TLS 1.2"
        }, new Project
        {
            CreatedBy = "Giorgi",
            Id = 2,
            Name = "Move Blog to Hugo"
        });

        modelBuilder.Entity<TimeLog>().HasData(new TimeLog
        {
            Id = 1,
            From = new DateTime(2020, 7, 24, 12, 0, 0),
            To = new DateTime(2020, 7, 24, 14, 0, 0),
            ProjectId = 1,
            User = "Giorgi"
        }, new TimeLog
        {
            Id = 2,
            From = new DateTime(2020, 7, 24, 16, 0, 0),
            To = new DateTime(2020, 7, 24, 18, 0, 0),
            ProjectId = 1,
            User = "Giorgi"
        }, new TimeLog
        {
            Id = 3,
            From = new DateTime(2020, 7, 24, 20, 0, 0),
            To = new DateTime(2020, 7, 24, 22, 0, 0),
            ProjectId = 2,
            User = "Giorgi"
        });

        base.OnModelCreating(modelBuilder);
    }
}




